<!--
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
#  KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
-->

# Apache Mynewt on the Texas Instruments CC2650 

## Overview

This repository will someday be a working port of the Texas Instruments CC2650 to the Apache Mynewt OS. For those of you who don't know, the CC2650 is a Cortex-M3 processor with a multi-protocol 2.4 GHz radio module that's controlled with a Cortex-M0 co-processor. Multi-protocol means it can handle a bunch of different standards like 802.15.4 or Bluetooth. Co-processor means there's another CPU on board who's only job is to handle the complexities of implementing the aforementioned standards' MAC layer, which is super convenient.

The only problem is, none of the HAL for the CC2650 is written. So that's what this is.

## Special Thanks To

My heart goes out to the people that made the NRF52 MCU specification, your template is much appreciated! Also thanks to Leon George for the initial peripheral includes, which I bummed off RIOT OS. 

## Building

For some reason I couldnt get the mynewt core to install until I appended the `-f` command so when you clone this repo (and after [you've installed newt](https://mynewt.apache.org/develop/get_started/native_install/index.html)), run this command inside the repo to install the mynewt core. 

```no-highlight
    $ newt install -f -v 
```

Then just run

```no-highlight
    $ newt build boot-cc2650
```

and hopefully it works. If it doesn't, you might have to get your hands dirty!

