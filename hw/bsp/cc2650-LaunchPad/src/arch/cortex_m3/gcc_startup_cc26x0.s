/*
Copyright (c) 2015, Nordic Semiconductor ASA
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of Nordic Semiconductor ASA nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
NOTE: Template files (including this one) are application specific and therefore
expected to be copied into the application project folder prior to its use!
*/

    .syntax unified
    .arch armv7-m

    .section .stack
    .align 3
    .equ    Stack_Size, 432
    .globl    __StackTop
    .globl    __StackLimit
__StackLimit:
    .space    Stack_Size
    .size __StackLimit, . - __StackLimit
__StackTop:
    .size __StackTop, . - __StackTop

    .section .heap
    .align 3
#ifdef __HEAP_SIZE
    .equ    Heap_Size, __HEAP_SIZE
#else
    .equ    Heap_Size, 0
#endif
    .globl    __HeapBase
    .globl    __HeapLimit
__HeapBase:
    .if    Heap_Size
    .space    Heap_Size
    .endif
    .size __HeapBase, . - __HeapBase
__HeapLimit:
    .size __HeapLimit, . - __HeapLimit

    .section .isr_vector
    .align 2
    .globl __isr_vector
__isr_vector:
    .long    __StackTop            /* Top of Stack */
    .long   Reset_Handler               /* Reset Handler */
    .long   NMI_Handler                 /* NMI Handler */
    .long   HardFault_Handler           /* Hard Fault Handler */
    .long   0                           /* Reserved */
    .long   0                           /* Reserved */
    .long   0                           /* Reserved */
    .long   0                           /* Reserved */
    .long   0                           /* Reserved */
    .long   0                           /* Reserved */
    .long   0                           /* Reserved */
    .long   SVC_Handler                 /* SVCall Handler */
    .long   0                           /* Reserved */
    .long   0                           /* Reserved */
    .long   PendSV_Handler              /* PendSV Handler */
    .long   SysTick_Handler             /* SysTick Handler */

  /* External Interrupts */
    .long   EDGE_DETECT_IRQHandler
    .long   I2C_IRQHandler
    .long   RF_CPE1_IRQHandler
    .long   SPIS_IRQHandler
    .long   AON_RTC_IRQHandler
    .long   UART0_IRQHandler
    .long   AON_AUX_SWEV0_IRQHandler
    .long   SSI0_IRQHandler
    .long   SSI1_IRQHandler
    .long   RF_CPE0_IRQHandler
    .long   RF_HW_IRQHandler
    .long   RF_CMD_ACK_IRQHandler
    .long   I2S_IRQHandler
    .long   AON_AUX_SWEV1_IRQHandler
    .long   WATCHDOG_IRQHandler
    .long   GPTIMER_0A_IRQHandler
    .long   GPTIMER_0B_IRQHandler
    .long   GPTIMER_1A_IRQHandler
    .long   GPTIMER_1B_IRQHandler
    .long   GPTIMER_2A_IRQHandler
    .long   GPTIMER_2B_IRQHandler
    .long   GPTIMER_3A_IRQHandler
    .long   GPTIMER_3B_IRQHandler
    .long   CRYPTO_IRQHandler
    .long   UDMA_IRQHandler
    .long   UDMA_ERR_IRQHandler
    .long   FLASH_CTRL_IRQHandler
    .long   SW0_IRQHandler
    .long   AUX_COMBO_IRQHandler
    .long   AON_PRG0_IRQHandler
    .long   PROG_IRQHandler
    .long   AUX_COMPA_IRQHandler
    .long   AUX_ADC_IRQHandler
    .long   TRNG_IRQHandler

    .size    __isr_vector, . - __isr_vector

/* Reset Handler */

    .text
    .thumb
    .thumb_func
    .align 1
    .globl    Reset_Handler
    .type    Reset_Handler, %function
Reset_Handler:
    .fnstart

    /* Clear BSS */
    mov     r0, #0
    ldr     r2, =__bss_start__
    ldr     r3, =__bss_end__
.bss_zero_loop:
    cmp     r2, r3
    itt     lt
    strlt   r0, [r2], #4
    blt    .bss_zero_loop


/*     Loop to copy data from read only memory to RAM. The ranges
 *      of copy from/to are specified by following symbols evaluated in
 *      linker script.
 *      __etext: End of code section, i.e., begin of data sections to copy from.
 *      __data_start__/__data_end__: RAM address range that data should be
 *      copied to. Both must be aligned to 4 bytes boundary.  */
    ldr    r1, =__etext
    ldr    r2, =__data_start__
    ldr    r3, =__data_end__

    subs    r3, r2
    ble     .LC0

.LC1:
    subs    r3, 4
    ldr    r0, [r1,r3]
    str    r0, [r2,r3]
    bgt    .LC1

.LC0:

    LDR     R0, =__HeapBase
    LDR     R1, =__HeapLimit
    BL      _sbrkInit

    LDR     R0, =SystemInit
    BLX     R0

    BL      hal_system_init

    LDR     R0, =_start
    BX      R0

    .pool
    .cantunwind
    .fnend
    .size   Reset_Handler,.-Reset_Handler

    .section ".text"


/* Dummy Exception Handlers (infinite loops which can be modified) */

    .weak   NMI_Handler
    .type   NMI_Handler, %function
NMI_Handler:
    B       .
    .size   NMI_Handler, . - NMI_Handler


    .weak   HardFault_Handler
    .type   HardFault_Handler, %function
HardFault_Handler:
    B       .
    .size   HardFault_Handler, . - HardFault_Handler


    .weak   MemoryManagement_Handler
    .type   MemoryManagement_Handler, %function
MemoryManagement_Handler:
    B       .
    .size   MemoryManagement_Handler, . - MemoryManagement_Handler


    .weak   BusFault_Handler
    .type   BusFault_Handler, %function
BusFault_Handler:
    B       .
    .size   BusFault_Handler, . - BusFault_Handler


    .weak   UsageFault_Handler
    .type   UsageFault_Handler, %function
UsageFault_Handler:
    B       .
    .size   UsageFault_Handler, . - UsageFault_Handler


    .weak   SVC_Handler
    .type   SVC_Handler, %function
SVC_Handler:
    B       .
    .size   SVC_Handler, . - SVC_Handler


    .weak   PendSV_Handler
    .type   PendSV_Handler, %function
PendSV_Handler:
    B       .
    .size   PendSV_Handler, . - PendSV_Handler


    .weak   SysTick_Handler
    .type   SysTick_Handler, %function
SysTick_Handler:
    B       .
    .size   SysTick_Handler, . - SysTick_Handler


/* IRQ Handlers */

    .globl  Default_Handler
    .type   Default_Handler, %function
Default_Handler:
    B       .
    .size   Default_Handler, . - Default_Handler

    .macro  IRQ handler
    .weak   \handler
    .set    \handler, Default_Handler
    .endm

    IRQ  EDGE_DETECT_IRQHandler
    IRQ  I2C_IRQHandler
    IRQ  RF_CPE1_IRQHandler
    IRQ  SPIS_IRQHandler
    IRQ  AON_RTC_IRQHandler
    IRQ  UART0_IRQHandler
    IRQ  AON_AUX_SWEV0_IRQHandler
    IRQ  SSI0_IRQHandler
    IRQ  SSI1_IRQHandler
    IRQ  RF_CPE0_IRQHandler
    IRQ  RF_HW_IRQHandler
    IRQ  RF_CMD_ACK_IRQHandler
    IRQ  I2S_IRQHandler
    IRQ  AON_AUX_SWEV1_IRQHandler
    IRQ  WATCHDOG_IRQHandler
    IRQ  GPTIMER_0A_IRQHandler
    IRQ  GPTIMER_0B_IRQHandler
    IRQ  GPTIMER_1A_IRQHandler
    IRQ  GPTIMER_1B_IRQHandler
    IRQ  GPTIMER_2A_IRQHandler
    IRQ  GPTIMER_2B_IRQHandler
    IRQ  GPTIMER_3A_IRQHandler
    IRQ  GPTIMER_3B_IRQHandler
    IRQ  CRYPTO_IRQHandler
    IRQ  UDMA_IRQHandler
    IRQ  UDMA_ERR_IRQHandler
    IRQ  FLASH_CTRL_IRQHandler
    IRQ  SW0_IRQHandler
    IRQ  AUX_COMBO_IRQHandler
    IRQ  AON_PRG0_IRQHandler
    IRQ  PROG_IRQHandler
    IRQ  AUX_COMPA_IRQHandler
    IRQ  AUX_ADC_IRQHandler
    IRQ  TRNG_IRQHandler

  .end
