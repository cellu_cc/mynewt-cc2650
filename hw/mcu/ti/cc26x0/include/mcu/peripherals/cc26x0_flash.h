/******************************************************************************
*  Filename:       hw_flash_h
*  Revised:        2016-03-14 09:20:59 +0100 (Mon, 14 Mar 2016)
*  Revision:       45924
*
* Copyright (c) 2015 - 2016, Texas Instruments Incorporated
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1) Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2) Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3) Neither the name of the ORGANIZATION nor the names of its contributors may
*    be used to endorse or promote products derived from this software without
*    specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************/

#ifndef __HW_FLASH_H__
#define __HW_FLASH_H__

//*****************************************************************************
//
// Values that can be returned from the API functions
//
//*****************************************************************************
#define FAPI_STATUS_SUCCESS     0x00000000  // Function completed successfully
#define FAPI_STATUS_FSM_BUSY    0x00000001  // FSM is Busy
#define FAPI_STATUS_FSM_READY   0x00000002  // FSM is Ready
#define FAPI_STATUS_INCORRECT_DATABUFFER_LENGTH \
                                0x00000003  // Incorrect parameter value
#define FAPI_STATUS_FSM_ERROR   0x00000004  // Operation failed

                                
#define FLASH_CTRL_BASE 0x40030000

//*****************************************************************************
//
// This section defines the register offsets of
// FLASH component
//
//*****************************************************************************
// FMC and Efuse Status
#define FLASH_O_STAT                                                0x0000001C

// Flash Configuration
#define FLASH_O_CFG                                                 0x00000024

// Register for finding the size of the flash bank
#define FLASH_O_FLASH_SIZE                                          0x0000002C

// Used for finding the size of a sector
#define FLASH_O_FCFG_B0_SSIZE0                                      0x00002430



//*****************************************************************************
//
// Register: FLASH_O_STAT
//
//*****************************************************************************
// Field:    [15] EFUSE_BLANK
//
// Efuse scanning detected if fuse ROM is blank:
// 0 : Not blank
// 1 : Blank
#define FLASH_STAT_EFUSE_BLANK                                      0x00008000
#define FLASH_STAT_EFUSE_BLANK_BITN                                         15
#define FLASH_STAT_EFUSE_BLANK_M                                    0x00008000
#define FLASH_STAT_EFUSE_BLANK_S                                            15

// Field:    [14] EFUSE_TIMEOUT
//
// Efuse scanning resulted in timeout error.
// 0 : No Timeout error
// 1 : Timeout Error
#define FLASH_STAT_EFUSE_TIMEOUT                                    0x00004000
#define FLASH_STAT_EFUSE_TIMEOUT_BITN                                       14
#define FLASH_STAT_EFUSE_TIMEOUT_M                                  0x00004000
#define FLASH_STAT_EFUSE_TIMEOUT_S                                          14

// Field:    [13] EFUSE_CRC_ERROR
//
// Efuse scanning resulted in scan chain CRC error.
// 0 : No CRC error
// 1 : CRC Error
#define FLASH_STAT_EFUSE_CRC_ERROR                                  0x00002000
#define FLASH_STAT_EFUSE_CRC_ERROR_BITN                                     13
#define FLASH_STAT_EFUSE_CRC_ERROR_M                                0x00002000
#define FLASH_STAT_EFUSE_CRC_ERROR_S                                        13

// Field:  [12:8] EFUSE_ERRCODE
//
// Same as EFUSEERROR.CODE
#define FLASH_STAT_EFUSE_ERRCODE_W                                           5
#define FLASH_STAT_EFUSE_ERRCODE_M                                  0x00001F00
#define FLASH_STAT_EFUSE_ERRCODE_S                                           8

// Field:     [2] SAMHOLD_DIS
//
// Status indicator of flash sample and hold sequencing logic. This bit will go
// to 1 some delay after CFG.DIS_IDLE is set to 1.
// 0: Not disabled
// 1: Sample and hold disabled and stable
#define FLASH_STAT_SAMHOLD_DIS                                      0x00000004
#define FLASH_STAT_SAMHOLD_DIS_BITN                                          2
#define FLASH_STAT_SAMHOLD_DIS_M                                    0x00000004
#define FLASH_STAT_SAMHOLD_DIS_S                                             2

// Field:     [1] BUSY
//
// Fast version of the FMC FMSTAT.BUSY bit.
// This flag is valid immediately after the operation setting it (FMSTAT.BUSY
// is delayed some cycles)
// 0 : Not busy
// 1 : Busy
#define FLASH_STAT_BUSY                                             0x00000002
#define FLASH_STAT_BUSY_BITN                                                 1
#define FLASH_STAT_BUSY_M                                           0x00000002
#define FLASH_STAT_BUSY_S                                                    1
#define FLASH_STAT_NOT_BUSY                                                  0

// Field:     [0] POWER_MODE
//
// Power state of the flash sub-system.
// 0 : Active
// 1 : Low power
#define FLASH_STAT_POWER_MODE                                       0x00000001
#define FLASH_STAT_POWER_MODE_BITN                                           0
#define FLASH_STAT_POWER_MODE_M                                     0x00000001
#define FLASH_STAT_POWER_MODE_S                                              0

//*****************************************************************************
//
// Register: FLASH_O_CFG
//
//*****************************************************************************
// Field:     [8] STANDBY_MODE_SEL
//
// Internal. Only to be used through TI provided API.
#define FLASH_CFG_STANDBY_MODE_SEL                                  0x00000100
#define FLASH_CFG_STANDBY_MODE_SEL_BITN                                      8
#define FLASH_CFG_STANDBY_MODE_SEL_M                                0x00000100
#define FLASH_CFG_STANDBY_MODE_SEL_S                                         8

// Field:   [7:6] STANDBY_PW_SEL
//
// Internal. Only to be used through TI provided API.
#define FLASH_CFG_STANDBY_PW_SEL_W                                           2
#define FLASH_CFG_STANDBY_PW_SEL_M                                  0x000000C0
#define FLASH_CFG_STANDBY_PW_SEL_S                                           6

// Field:     [5] DIS_EFUSECLK
//
// Internal. Only to be used through TI provided API.
#define FLASH_CFG_DIS_EFUSECLK                                      0x00000020
#define FLASH_CFG_DIS_EFUSECLK_BITN                                          5
#define FLASH_CFG_DIS_EFUSECLK_M                                    0x00000020
#define FLASH_CFG_DIS_EFUSECLK_S                                             5

// Field:     [4] DIS_READACCESS
//
// Internal. Only to be used through TI provided API.
#define FLASH_CFG_DIS_READACCESS                                    0x00000010
#define FLASH_CFG_DIS_READACCESS_BITN                                        4
#define FLASH_CFG_DIS_READACCESS_M                                  0x00000010
#define FLASH_CFG_DIS_READACCESS_S                                           4

// Field:     [3] ENABLE_SWINTF
//
// Internal. Only to be used through TI provided API.
#define FLASH_CFG_ENABLE_SWINTF                                     0x00000008
#define FLASH_CFG_ENABLE_SWINTF_BITN                                         3
#define FLASH_CFG_ENABLE_SWINTF_M                                   0x00000008
#define FLASH_CFG_ENABLE_SWINTF_S                                            3

// Field:     [1] DIS_STANDBY
//
// Internal. Only to be used through TI provided API.
#define FLASH_CFG_DIS_STANDBY                                       0x00000002
#define FLASH_CFG_DIS_STANDBY_BITN                                           1
#define FLASH_CFG_DIS_STANDBY_M                                     0x00000002
#define FLASH_CFG_DIS_STANDBY_S                                              1

// Field:     [0] DIS_IDLE
//
// Internal. Only to be used through TI provided API.
#define FLASH_CFG_DIS_IDLE                                          0x00000001
#define FLASH_CFG_DIS_IDLE_BITN                                              0
#define FLASH_CFG_DIS_IDLE_M                                        0x00000001
#define FLASH_CFG_DIS_IDLE_S                                                 0


//*****************************************************************************
//
// Register: FLASH_O_FLASH_SIZE
//
//*****************************************************************************
// Field:   [7:0] SECTORS
//
// Internal. Only to be used through TI provided API.
#define FLASH_FLASH_SIZE_SECTORS_W                                           8
#define FLASH_FLASH_SIZE_SECTORS_M                                  0x000000FF
#define FLASH_FLASH_SIZE_SECTORS_S                                           0

//*****************************************************************************
//
// Register: FLASH_O_FCFG_B0_SSIZE0
//
//*****************************************************************************
// Field: [27:16] B0_NUM_SECTORS
//
// Internal. Only to be used through TI provided API.
#define FLASH_FCFG_B0_SSIZE0_B0_NUM_SECTORS_W                               12
#define FLASH_FCFG_B0_SSIZE0_B0_NUM_SECTORS_M                       0x0FFF0000
#define FLASH_FCFG_B0_SSIZE0_B0_NUM_SECTORS_S                               16

// Field:   [3:0] B0_SECT_SIZE
//
// Internal. Only to be used through TI provided API.
#define FLASH_FCFG_B0_SSIZE0_B0_SECT_SIZE_W                                  4
#define FLASH_FCFG_B0_SSIZE0_B0_SECT_SIZE_M                         0x0000000F
#define FLASH_FCFG_B0_SSIZE0_B0_SECT_SIZE_S                                  0

__STATIC_INLINE uint32_t
FlashSectorSizeGet(void)
{
    uint32_t ui32SectorSizeInKbyte;

    ui32SectorSizeInKbyte = (HWREG(FLASH_CTRL_BASE + FLASH_O_FCFG_B0_SSIZE0) &
                             FLASH_FCFG_B0_SSIZE0_B0_SECT_SIZE_M) >>
                             FLASH_FCFG_B0_SSIZE0_B0_SECT_SIZE_S;

    //
    // Return flash sector size in number of bytes.
    //
    return(ui32SectorSizeInKbyte * 1024);
}



__STATIC_INLINE uint32_t
FlashSizeGet(void)
{
    uint32_t ui32NoOfSectors;

    //
    // Get number of flash sectors
    //
    ui32NoOfSectors = (HWREG(FLASH_CTRL_BASE + FLASH_O_FLASH_SIZE) &
                       FLASH_FLASH_SIZE_SECTORS_M) >>
                       FLASH_FLASH_SIZE_SECTORS_S;

    //
    // Return flash size in number of bytes
    //
    return(ui32NoOfSectors * FlashSectorSizeGet());
}



#endif // __FLASH__
