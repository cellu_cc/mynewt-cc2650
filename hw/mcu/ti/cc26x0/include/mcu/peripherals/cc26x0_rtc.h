/*
 * Copyright (C) 2016 Leon George
 *
 * This file is subject to the terms and conditions of the GNU Lesser
 * General Public License v2.1. See the file LICENSE in the top level
 * directory for more details.
 */

/**
 * @ingroup         cpu_cc26x0_definitions
 * @{
 *
 * @file
 * @brief           CC26x0 PRCM register definitions
 */

#ifndef CC26X0_RTC_H
#define CC26X0_RTC_H

#include "mcu/cc26x0.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * AON_RTC.CTL Register
 * This register contains various bitfields 
 * for configuration of RTC
 */

typedef struct{
    reg8_t reserved;
    reg8_t COMB_EV_MASK;
    reg8_t EV_DELAY;
    reg8_t RESET_AND_ENABLE;
}aon_rtc_ctl_reg_t;

/*
 * CTL Register values
 */

/*
 * COMB_EV_MASK: Eventmask selecting which delayed 
 *               events form the combined event.
 */ 
#define AON_RTC_CTL_COMB_EV_MASK_NO_EVENT    0x0 
#define AON_RTC_CTL_COMB_EV_MASK_USE_CHAN_0  0x1
#define AON_RTC_CTL_COMB_EV_MASK_USE_CHAN_1  0x2
#define AON_RTC_CTL_COMB_EV_MASK_USE_CHAN_2  0x4

/*
 * EV_DELAY: Number of SCLK_LF clock cycles waited 
 *           before generating delayed events. 
 *           (Common setting for all RTC cannels) 
 *           the delayed event is delayed
 */ 

#define AON_RTC_CTL_EV_DELAY_000    0x0
#define AON_RTC_CTL_EV_DELAY_001    0x1
#define AON_RTC_CTL_EV_DELAY_002    0x2
#define AON_RTC_CTL_EV_DELAY_004    0x3
#define AON_RTC_CTL_EV_DELAY_008    0x4
#define AON_RTC_CTL_EV_DELAY_016    0x5
#define AON_RTC_CTL_EV_DELAY_032    0x6
#define AON_RTC_CTL_EV_DELAY_048    0x7
#define AON_RTC_CTL_EV_DELAY_064    0x8
#define AON_RTC_CTL_EV_DELAY_080    0x9
#define AON_RTC_CTL_EV_DELAY_096    0xA
#define AON_RTC_CTL_EV_DELAY_112    0xB
#define AON_RTC_CTL_EV_DELAY_128    0xC
#define AON_RTC_CTL_EV_DELAY_144    0xD

/*
 * RESET_AND_ENABLE: Register for optionally resetting
 *                   the RTC and enabling other features 
 */

#define AON_RTC_CTL_RESET_POSITION   0x7
#define AON_RTC_CTL_RTC_RESET       (0x1 << AON_RTC_CTL_RESET_POSITION)  

/* RTC_4KHZ is a 4 KHz reference output, 
 * tapped from SUBSEC.VALUE bit 19 which
 * is used by AUX timer.
 */
#define AON_RTC_CTL_4KHZ_EN_POSITION 0x2
#define AON_RTC_CTL_4KHZ_EN         (0x1 << AON_RTC_CTL_4KHZ_EN_POSITION)  

/* RTC_UPD is a 16 KHz signal used to sync 
 * up the radio timer. The 16 Khz is SCLK_LF 
 * divided by 2
 */
#define AON_RTC_CTL_UPD_EN_POSITION  0x1
#define AON_RTC_CTL_UPD_EN          (0x1 << AON_RTC_CTL_UPD_EN_POSITION)  

#define AON_RTC_CTL_RTC_ENABLE       0x1

/*
 * AON_RTC.EVFLAGS Register 
 * This register contains event flags from the 3 RTC 
 * channels. Each flag will be cleared when writing 
 * a '1' to the corresponding bitfield
 */
typedef struct {
    reg8_t reserved;
    reg8_t CH2;
    reg8_t CH1;
    reg8_t CH0;
} aon_rtc_evflags_reg_t;


/* NB: CHx refers to CH0, CH1, and CH2*/

/*
 * CHx: Channel x event flag, set when CHCTL.CHx_EN = 1 
 *      and the RTC value matches or passes the CHxCMP 
 *      value. An event will be scheduled to occur as soon
 *      as possible when writing to CHxCMP provided that
 *      the channel is enabled and the new value matches 
 *      any time between next RTC value and 1 second in 
 *      the past.
 *      Writing 1 clears this flag. Note that a new event 
 *      can not occur on this channel in first 2 SCLK_LF 
 *      cycles after a clearance.
 * For CH2:
 *      AUX_SCE can read the flag through AUX_WUC:WUEVFLAGS.AON_RTC_CH2 
 *      and clear it using AUX_WUC:WUEVCLR.AON_RTC_CH2.
 * For CH1:
 *      The CH1 event flag can also set when CHCTL.CH1_CAPT_EN = 1
 *      and capture occurs. 
 */

#define AON_RTC_EVFLAGS_CHX_MASK       (~(0x1))
#define AON_RTC_EVFLAGS_CHX_CLEAR_FLAG (0x1)

/* 
 * AON_RTC.SEC and AON_RTC.SUBSEC Registers
 * 
 * AON_RTC.SEC:
 * Unsigned integer representing Real Time 
 * Clock in seconds. When reading this register 
 * the content of SUBSEC.VALUE is simultaneously 
 * latched. A consistent reading of the combined 
 * Real Time Clock can be obtained by first reading 
 * this register, then reading SUBSEC register.
 *
 * AON_RTC.SUBSEC:
 * Unsigned integer representing Real Time Clock 
 * in fractions of a second (VALUE/2^32 seconds) 
 * at the time when SEC register was read.
 * Examples :
 *   - 0x0000_0000 = 0.0 sec
 *   - 0x4000_0000 = 0.25 sec 
 *   - 0x8000_0000 = 0.5 sec
 *   - 0xC000_0000 = 0.75 sec
 */

/* 
 * AON_RTC.SUBSECINC Register
 * 
 * Value added to SUBSEC.VALUE on every SCLK_LFclock 
 * cycle. This value compensates for a SCLK_LF clock 
 * which has an offset from 32768 Hz.
 *
 * The compensation value can be found as 2^38 / freq,
 * where freq is SCLK_LF clock frequency in Hertz
 * This value is added to SUBSEC.VALUE on every cycle, 
 * and carry of this is added to SEC.VALUE. To perform 
 * the addition, bits [23:6] are aligned with 
 * SUBSEC.VALUE bits [17:0]. 
 * The remaining bits [5:0] are accumulated in a hidden
 * 6-bit register that generates a carry into the above
 * mentioned addition on overflow.
 * The default value corresponds to incrementing by 
 * precisely 1/32768 of a second.
 * NOTE: This register is read only. Modification of 
 * the register value must be done using registers 
 * AUX_WUC:RTCSUBSECINC1 , AUX_WUC:RTCSUBSECINC0 and 
 * AUX_WUC:RTCSUBSECINCCTL
 */

#define AON_RTC_SUBSECINC_MASK (0xFFFFFF)

/*
 * AON_RTC.CHCTL Register 
 * Channel Configuration
 */

typedef struct{
    uint8_t reserved;
    uint8_t CH2_CONF;
    uint8_t CH1_CONF;
    uint8_t CH0_CONF;
} aon_rtc_chctl_reg_t;

/* Set to enable continuous operation of Channel 2 */
#define AON_RTC_CHCTL_CH2_CONT_ENABLE (0x1 << 2)
/* Set Channel 1 Capture (1) or Compare (0) Mode */
#define AON_RTC_CHCTL_CH1_CAPT_ENABLE (0x1 << 1)
/* Enable Channel x */
#define AON_RTC_CHCTL_CHX_ENABLE (0x1)


/*
 * AON_RTC registers
 */
typedef struct {
        aon_rtc_ctl_reg_t CTL;       /* Control */
    aon_rtc_evflags_reg_t EVFLAGS;   /* Event Flags, RTC Status */
                  reg32_t SEC;       /* Second Counter Value, Integer Part */
                  reg32_t SUBSEC;    /* Second Counter Value, Fractional Part */
                  reg32_t SUBSECINC; /* Subseconds Increment */
      aon_rtc_chctl_reg_t CHCTL;     /* Channel Configuration */
                  reg32_t CH0CMP;    /* Channel 0 Compare Value */
                  reg32_t CH1CMP;    /* Channel 1 Compare Value */
                  reg32_t CH2CMP;    /* Channel 2 Compare Value */
                  reg32_t CH2CMPINC; /* Channel 2 Compare Value Auto-Increment */
                  reg32_t CH1CAPT;   /* Channel 1 Capture Value */
                  reg32_t SYNC;      /* AON Synchronization */
} aon_rtc_regs_t;

#define AON_RTC_BASE       0x40092000 /**< AON_RTC base address */

#define AON_RTC ((aon_rtc_regs_t *) (AON_RTC_BASE)) /**< AON_RTC register bank */

#ifdef __cplusplus
} /* end extern "C" */
#endif

#endif /* CC26X0_PRCM_H */

/*@}*/
