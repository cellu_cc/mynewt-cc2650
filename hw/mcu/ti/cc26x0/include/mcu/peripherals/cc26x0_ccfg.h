/*
 * Copyright (C) 2016 Leon George
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */
/**
 * @ingroup         cpu_cc26x0_definitions
 * @{
 *
 * @file
 * @brief           CC26x0 CCFG register definitions
 */

#ifndef CC26X0_CCFG_H
#define CC26X0_CCFG_H

#include "mcu/cc26x0.h"

#ifdef __cplusplus
extern "C" {
#endif

/** @ingroup cpu_specific_peripheral_memory_map
 * @{
 */
#define CCFG_BASE                           0x50003000 /**<  base address of CCFG memory */
/*@}*/

/* TODO not present in datasheet but explained in cc26x0ware-lds */
#define CCFG_SIZE_AND_DIS_FLAGS_DIS_GPRAM   0x00000004 /**< CCFG_SIZE_AND_DIS_FLAGS_DIS_GPRAM */

/**
 * CCFG registers
 */
typedef struct {
    reg32_t EXT_LF_CLK; /**< extern LF clock config */
    reg32_t MODE_CONF_1; /**< mode config 1 */
    reg32_t SIZE_AND_DIS_FLAGS; /**< CCFG size and disable flags */
    reg32_t MODE_CONF; /**< mmode config 0 */
    reg32_t VOLT_LOAD_0; /**< voltage load 0 */
    reg32_t VOLT_LOAD_1; /**< voltage load 1 */
    reg32_t RTC_OFFSET; /**< RTC offset */
    reg32_t FREQ_OFFSET; /**< frequency offset */
    reg32_t IEEE_MAC_0; /**< IEEE MAC address 0 */
    reg32_t IEEE_MAC_1; /**< IEEE MAC address 1 */
    reg32_t IEEE_BLE_0; /**< IEEE BLE address 0 */
    reg32_t IEEE_BLE_1; /**< IEEE BLE address 1 */
    reg32_t BL_CONFIG; /**< bootloader config */
    reg32_t ERASE_CONF; /**< erase config */
    reg32_t CCFG_TI_OPTIONS; /**< TI options */
    reg32_t CCFG_TAP_DAP_0; /**< test access points enable 0 */
    reg32_t CCFG_TAP_DAP_1; /**< test access points enable 1 */
    reg32_t IMAGE_VALID_CONF; /**< image valid */
    reg32_t CCFG_PROT_31_0; /**< protect sectors 0-31 */
    reg32_t CCFG_PROT_63_32; /**< protect sectors 32-63 */
    reg32_t CCFG_PROT_95_64; /**< protect sectors 64-95 */
    reg32_t CCFG_PROT_127_96; /**< protect sectors 96-127 */
} ccfg_regs_t;

#define CCFG ((ccfg_regs_t *) (CCFG_BASE + 0xFA8)) /**< CCFG register bank */


typedef struct
{                                              //  Mapped to address
    uint32_t   CCFG_EXT_LF_CLK               ; // 0x50003FA8
    uint32_t   CCFG_MODE_CONF_1              ; // 0x50003FAC
    uint32_t   CCFG_SIZE_AND_DIS_FLAGS       ; // 0x50003FB0
    uint32_t   CCFG_MODE_CONF                ; // 0x50003FB4
    uint32_t   CCFG_VOLT_LOAD_0              ; // 0x50003FB8
    uint32_t   CCFG_VOLT_LOAD_1              ; // 0x50003FBC
    uint32_t   CCFG_RTC_OFFSET               ; // 0x50003FC0
    uint32_t   CCFG_FREQ_OFFSET              ; // 0x50003FC4
    uint32_t   CCFG_IEEE_MAC_0               ; // 0x50003FC8
    uint32_t   CCFG_IEEE_MAC_1               ; // 0x50003FCC
    uint32_t   CCFG_IEEE_BLE_0               ; // 0x50003FD0
    uint32_t   CCFG_IEEE_BLE_1               ; // 0x50003FD4
    uint32_t   CCFG_BL_CONFIG                ; // 0x50003FD8
    uint32_t   CCFG_ERASE_CONF               ; // 0x50003FDC
    uint32_t   CCFG_CCFG_TI_OPTIONS          ; // 0x50003FE0
    uint32_t   CCFG_CCFG_TAP_DAP_0           ; // 0x50003FE4
    uint32_t   CCFG_CCFG_TAP_DAP_1           ; // 0x50003FE8
    uint32_t   CCFG_IMAGE_VALID_CONF         ; // 0x50003FEC
    uint32_t   CCFG_CCFG_PROT_31_0           ; // 0x50003FF0
    uint32_t   CCFG_CCFG_PROT_63_32          ; // 0x50003FF4
    uint32_t   CCFG_CCFG_PROT_95_64          ; // 0x50003FF8
    uint32_t   CCFG_CCFG_PROT_127_96         ; // 0x50003FFC
} ccfg_t;

#ifdef __cplusplus
} /* end extern "C" */
#endif

#endif /* CC26X0_CCFG_H */

/*@}*/
