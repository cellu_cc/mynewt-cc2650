#ifndef __CC26X0_ROM_H__
#define __CC26X0_ROM_H__

#define ROM_API_TABLE           ((uint32_t *) 0x10000180)
#define ROM_VERSION             (ROM_API_TABLE[0])

#define ROM_API_AON_EVENT_TABLE ((uint32_t*) (ROM_API_TABLE[1]))
#define ROM_API_AON_IOC_TABLE   ((uint32_t*) (ROM_API_TABLE[2]))
#define ROM_API_AON_RTC_TABLE   ((uint32_t*) (ROM_API_TABLE[3]))
#define ROM_API_AON_WUC_TABLE   ((uint32_t*) (ROM_API_TABLE[4]))
#define ROM_API_AUX_CTRL_TABLE  ((uint32_t*) (ROM_API_TABLE[5]))
#define ROM_API_AUX_TDC_TABLE   ((uint32_t*) (ROM_API_TABLE[6]))
#define ROM_API_AUX_TIMER_TABLE ((uint32_t*) (ROM_API_TABLE[7]))
#define ROM_API_AUX_WUC_TABLE   ((uint32_t*) (ROM_API_TABLE[8]))
#define ROM_API_DDI_TABLE       ((uint32_t*) (ROM_API_TABLE[9]))
#define ROM_API_FLASH_TABLE     ((uint32_t*) (ROM_API_TABLE[10]))
#define ROM_API_I2C_TABLE       ((uint32_t*) (ROM_API_TABLE[11]))
#define ROM_API_INTERRUPT_TABLE ((uint32_t*) (ROM_API_TABLE[12]))
#define ROM_API_IOC_TABLE       ((uint32_t*) (ROM_API_TABLE[13]))
#define ROM_API_PRCM_TABLE      ((uint32_t*) (ROM_API_TABLE[14]))
#define ROM_API_SMPH_TABLE      ((uint32_t*) (ROM_API_TABLE[15]))
#define ROM_API_SSI_TABLE       ((uint32_t*) (ROM_API_TABLE[17]))
#define ROM_API_TIMER_TABLE     ((uint32_t*) (ROM_API_TABLE[18]))
#define ROM_API_TRNG_TABLE      ((uint32_t*) (ROM_API_TABLE[19]))
#define ROM_API_UART_TABLE      ((uint32_t*) (ROM_API_TABLE[20]))
#define ROM_API_UDMA_TABLE      ((uint32_t*) (ROM_API_TABLE[21]))
#define ROM_API_VIMS_TABLE      ((uint32_t*) (ROM_API_TABLE[22]))

// FLASH FUNCTIONS
#define ROM_FlashPowerModeGet \
    ((uint32_t (*)(void)) \
    ROM_API_FLASH_TABLE[1])

#define ROM_FlashProtectionSet \
    ((void (*)(uint32_t ui32SectorAddress, uint32_t ui32ProtectMode)) \
    ROM_API_FLASH_TABLE[2])

#define ROM_FlashProtectionGet \
    ((uint32_t (*)(uint32_t ui32SectorAddress)) \
    ROM_API_FLASH_TABLE[3])

#define ROM_FlashProtectionSave \
    ((uint32_t (*)(uint32_t ui32SectorAddress)) \
    ROM_API_FLASH_TABLE[4])

#define ROM_FlashEfuseReadRow \
    ((bool (*)(uint32_t *pui32EfuseData, uint32_t ui32RowAddress)) \
    ROM_API_FLASH_TABLE[8])

#define ROM_FlashDisableSectorsForWrite \
    ((void (*)(void)) \
    ROM_API_FLASH_TABLE[9])



#endif