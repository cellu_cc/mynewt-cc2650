/* Copyright (c) 2012 ARM LIMITED
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   * Neither the name of ARM nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific
 *     prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdint.h>
#include <stdbool.h>
#include "bsp/cmsis_nvic.h"
//#include "nrf.h"

#include "mcu/cc26x0.h"
#include "mcu/system_cc26x0.h"


/*lint ++flb "Enter library region" */

#define __SYSTEM_CLOCK_48M      (48000000UL)


#if defined ( __CC_ARM )
    uint32_t SystemCoreClock __attribute__((used)) = __SYSTEM_CLOCK_48M;
#elif defined ( __ICCARM__ )
    __root uint32_t SystemCoreClock = __SYSTEM_CLOCK_48M;
#elif defined ( __GNUC__ )
    uint32_t SystemCoreClock __attribute__((used)) = __SYSTEM_CLOCK_48M;
#endif

#define CCFG_SIZE 88

const ccfg_t
ti_ccfg
__attribute__((section(".ti_ccfg")))
__attribute__((used)) = 
{
	0x00008001, /* EXT_LF_CLK: default values */
	0xFF13FFFF, /* MODE_CONF_1: default values */
	0x0058FFFF, /* SIZE_AND_DIS_FLAGS: 88 bytes long, no external osc. */
	0xFFFFFFFF, /* MODE_CONF: default values */
	0xFFFFFFFF, /* VOLT_LOAD_0: default values */
	0xFFFFFFFF, /* VOLT_LOAD_1: default values */
	0xFFFFFFFF, /* RTC_OFFSET: default values */
	0xFFFFFFFF, /* FREQ_OFFSET: default values */
	0xFFFFFFFF, /* IEEE_MAC_0: use MAC address from FCFG */
	0xFFFFFFFF, /* IEEE_MAC_1: use MAC address from FCFG */
	0xFFFFFFFF, /* IEEE_BLE_0: use BLE address from FCFG */
	0xFFFFFFFF, /* IEEE_BLE_1: use BLE address from FCFG */
	/* BL_CONFIG: disable backdoor and bootloader,
	 * default pin, default active level (high)
	 */
	0x00FFFF00,
	0xFFFFFFFF, /* ERASE_CONF: default values (banks + chip erase) */
	/* CCFG_TI_OPTIONS: disable TI failure analysis */
	0xFFFFFF00, /* reserved */
	0xFFC5C5C5, /* CCFG_TAP_DAP_0: default values */
	0xFFC5C5C5, /* CCFG_TAP_DAP_1: default values */
	/* IMAGE_VALID_CONF: authorize program on flash to run */
	0x00000000,
	/* Make all flash chip programmable + erasable
	 * (which is default)
	 */
	0xFFFFFFFF, /* CCFG_PROT_31_0 */
	0xFFFFFFFF, /* CCFG_PROT_61_32 */
	0xFFFFFFFF, /* CCFG_PROT_95_64 */
	0xFFFFFFFF  /* CCFG_PROT_127_96 */
};

void SystemCoreClockUpdate(void)
{
    SystemCoreClock = __SYSTEM_CLOCK_48M;
}

void SystemInit(void)
{
    SystemCoreClockUpdate();

    NVIC_Relocate();
}






/*lint --flb "Leave library region" */
