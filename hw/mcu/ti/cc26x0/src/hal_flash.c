/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string.h>
#include <assert.h>
#include "mcu/cc26x0.h"
#include "mcu/cc26x0_hal.h"
#include <hal/hal_flash_int.h>

#define CC26X0_FLASH_SECTOR_SZ	4096

static int cc26x0_flash_read(const struct hal_flash *dev, uint32_t address,
        void *dst, uint32_t num_bytes);
static int cc26x0_flash_write(const struct hal_flash *dev, uint32_t address,
        const void *src, uint32_t num_bytes);
static int cc26x0_flash_erase_sector(const struct hal_flash *dev,
        uint32_t sector_address);
static int cc26x0_flash_sector_info(const struct hal_flash *dev, int idx,
        uint32_t *address, uint32_t *sz);
static int cc26x0_flash_init(const struct hal_flash *dev);
static uint8_t disableFlashCache();
static void enableFlashCache(uint8_t mode);
static uint32_t VIMSModeGet();
static void VIMSModeSet(uint32_t ui32Mode);

static const struct hal_flash_funcs cc26x0_flash_funcs = {
    .hff_read = cc26x0_flash_read,
    .hff_write = cc26x0_flash_write,
    .hff_erase_sector = cc26x0_flash_erase_sector,
    .hff_sector_info = cc26x0_flash_sector_info,
    .hff_init = cc26x0_flash_init
};

const struct hal_flash cc26x0_flash_dev = {
    .hf_itf = &cc26x0_flash_funcs,
    .hf_base_addr = 0x00000000,
    .hf_size = 128 * 1024,	/* XXX read from factory info? */
    .hf_sector_cnt = 32,	/* XXX read from factory info? */
    .hf_align = 1
};


typedef uint32_t (* FlashPrgPointer_t) (uint8_t *, uint32_t, uint32_t);

typedef uint32_t (* FlashSectorErasePointer_t) (uint32_t);


#define CC26X0_FLASH_READY() ((HWREG(FLASH_CTRL_BASE + FLASH_O_STAT) & FLASH_STAT_BUSY) == FLASH_STAT_NOT_BUSY)

static int
cc26x0_flash_wait_ready(void)
{
    int i;

    for (i = 0; i < 100000; i++) {
        if (CC26X0_FLASH_READY()) {
            return 0;
        }
    }
    return -1;
}

static int
cc26x0_flash_read(const struct hal_flash *dev, uint32_t address, void *dst,
        uint32_t num_bytes)
{
    memcpy(dst, (void *)address, num_bytes);
    return 0;
}

/*
 * Flash write is done by writing 4 bytes at a time at a word boundary.
 */
static int
cc26x0_flash_write(const struct hal_flash *dev, uint32_t address,
        const void *src, uint32_t num_bytes)
{
    int sr;
    int rc = -1;
    uint8_t mode;
    uint32_t ui32ErrorReturn;
    FlashPrgPointer_t FuncPointer;

    if (cc26x0_flash_wait_ready()) {
        return -1;
    }
    __HAL_DISABLE_INTERRUPTS(sr);

    assert((address + num_bytes) <= (FLASH_BASE + FlashSizeGet()));
    
    mode = disableFlashCache();
    //
    // Call ROM function
    //
    FuncPointer = (uint32_t (*)(uint8_t *, uint32_t, uint32_t)) (ROM_API_FLASH_TABLE[6]);
    ui32ErrorReturn = FuncPointer( (uint8_t *)src, address, num_bytes);

    enableFlashCache(mode);

    if(ui32ErrorReturn != FAPI_STATUS_SUCCESS)
        return -1;
    //
    // Enable standby in flash bank since ROM function might have disabled it
    //
    HWREGBITW(FLASH_CTRL_BASE + FLASH_O_CFG, FLASH_CFG_DIS_STANDBY_BITN ) = 0;

    __HAL_ENABLE_INTERRUPTS(sr);
    rc = 0;
    return rc;
}

static int
cc26x0_flash_erase_sector(const struct hal_flash *dev, uint32_t sector_address)
{
    int sr;
    int rc = -1;
    uint8_t mode;
    uint32_t ui32ErrorReturn;
    FlashSectorErasePointer_t FuncPointer;

    if (cc26x0_flash_wait_ready()) {
        return -1;
    }
    __HAL_DISABLE_INTERRUPTS(sr);

    //
    // Check the arguments.
    //
    assert(sector_address <= (FLASH_BASE + FlashSizeGet() -
                                 FlashSectorSizeGet()));
    assert((sector_address & (FlashSectorSizeGet() - 1)) == 00);

    mode = disableFlashCache();

    //
    // Call ROM function
    //
    FuncPointer = (uint32_t (*)(uint32_t)) (ROM_API_FLASH_TABLE[5]);
    ui32ErrorReturn = FuncPointer(sector_address);

    enableFlashCache(mode);

    if(ui32ErrorReturn != FAPI_STATUS_SUCCESS)
        return -1;
    //
    // Enable standby in flash bank since ROM function migth have disabled it
    //
    HWREGBITW(FLASH_CTRL_BASE + FLASH_O_CFG, FLASH_CFG_DIS_STANDBY_BITN ) = 0;
    rc = 0;
    if (cc26x0_flash_wait_ready()) {
        return -1;
    }
    __HAL_ENABLE_INTERRUPTS(sr);
    
    return rc;
}

/*
 *  ======== disableFlashCache ========
 *  When updating the Flash, the VIMS (Vesatile Instruction Memory System)
 *  mode must be set to GPRAM or OFF, before programming, and both VIMS
 *  flash line buffers must be set to disabled.
 */
static uint8_t disableFlashCache(void)
{
    uint8_t mode = VIMSModeGet();

    if (mode != VIMS_MODE_DISABLED) {
        VIMSModeSet(VIMS_MODE_DISABLED);
        while (VIMSModeGet() != VIMS_MODE_DISABLED);
    }

    return (mode);
}

/*
 *  ======== enableFlashCache ========
 */
static void enableFlashCache(uint8_t mode)
{
    if (mode != VIMS_MODE_DISABLED) {
        VIMSModeSet(VIMS_MODE_ENABLED);
    }
}

static uint32_t
VIMSModeGet()
{
    //
    // Check the arguments.
    //
    if(VIMS->STAT & VIMS_STAT_MODE_CHANGING)
    {
        return (VIMS_MODE_CHANGING);
    }
    else
    {
        return (VIMS->STAT & VIMS_STAT_MODE_M);
    }


}

//*****************************************************************************
//
// Set the operational mode of the VIMS
//
//*****************************************************************************
static void
VIMSModeSet(uint32_t ui32Mode)
{
    //
    // Check the arguments.
    //

    assert((ui32Mode == VIMS_MODE_DISABLED)   ||
           (ui32Mode == VIMS_MODE_ENABLED)    ||
           (ui32Mode == VIMS_MODE_OFF));

    //
    // Set the mode.
    //
    VIMS->STAT &= ~VIMS_CTL_MODE_m;
    VIMS->STAT |= (ui32Mode & VIMS_CTL_MODE_m);
}

static int
cc26x0_flash_sector_info(const struct hal_flash *dev, int idx,
        uint32_t *address, uint32_t *sz)
{
    assert(idx < cc26x0_flash_dev.hf_sector_cnt);
    *address = idx * CC26X0_FLASH_SECTOR_SZ;
    *sz = CC26X0_FLASH_SECTOR_SZ;
    return 0;
}

static int
cc26x0_flash_init(const struct hal_flash *dev)
{
    return 0;
}
