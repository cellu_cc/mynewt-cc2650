/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string.h>
#include <stdint.h>
#include <assert.h>
#include <errno.h>
#include "syscfg/syscfg.h"
#include "bsp/cmsis_nvic.h"
#include "hal/hal_timer.h"
#include "os/os_trace_api.h"
#include "mcu/cc26x0.h"
#include "mcu/cc26x0_hal.h"

/* IRQ prototype */
typedef void (*hal_timer_irq_handler_t)(void);

/* Maximum number of hal timers used */
#define CC26X0_HAL_TIMER_MAX     (4)

/* Maximum timer frequency */
#define CC26X0_MAX_TIMER_FREQ    (6000000)

/*This is the code, not the amount*/
#define CC26X0_MAX_TIMER_FREQ_PRESCALER_CODE (0x2) /*Divide by 4*/

struct cc26x0_hal_timer {
    uint8_t tmr_enabled;
    uint8_t tmr_irq_num;
    uint8_t tmr_rtc;
    uint8_t tmr_pad;
    uint32_t tmr_cntr;
    uint32_t timer_isrs;
    uint32_t tmr_freq;
    void *tmr_reg;
    TAILQ_HEAD(hal_timer_qhead, hal_timer) hal_timer_q;
};

#if MYNEWT_VAL(TIMER_0)
struct cc26x0_hal_timer cc26x0_hal_timer0;
#endif
#if MYNEWT_VAL(TIMER_1)
struct cc26x0_hal_timer cc26x0_hal_timer1;
#endif
#if MYNEWT_VAL(TIMER_2)
struct cc26x0_hal_timer cc26x0_hal_timer2;
#endif
#if MYNEWT_VAL(TIMER_3)
struct cc26x0_hal_timer cc26x0_hal_timer3;
#endif

static const struct cc26x0_hal_timer *cc26x0_hal_timers[CC26X0_HAL_TIMER_MAX] = {
#if MYNEWT_VAL(TIMER_0)
    &cc26x0_hal_timer0,
#else
    NULL,
#endif
#if MYNEWT_VAL(TIMER_1)
    &cc26x0_hal_timer1,
#else
    NULL,
#endif
#if MYNEWT_VAL(TIMER_2)
    &cc26x0_hal_timer2,
#else
    NULL,
#endif
#if MYNEWT_VAL(TIMER_3)
    &cc26x0_hal_timer3,
#else
    NULL
#endif
};

/* Resolve timer number into timer structure */
#define CC26X0_HAL_TIMER_RESOLVE(__n, __v)       \
    if ((__n) >= CC26X0_HAL_TIMER_MAX) {         \
        rc = EINVAL;                            \
        goto err;                               \
    }                                           \
    (__v) = (struct cc26x0_hal_timer *) cc26x0_hal_timers[(__n)];            \
    if ((__v) == NULL) {                        \
        rc = EINVAL;                            \
        goto err;                               \
    }

/* Interrupt mask for interrupt enable/clear */
#define CC26X0_TIMER_INT_MASK(x)    ((1 << (uint32_t)(x)) << 16)

static uint32_t
cc26x0_read_timer_cntr(gpt_reg_t *hwtimer)
{
    /* Mask to make 16 bits wide*/
    return (hwtimer->TAV)&0xFFFFFFFF;
}

/**
 * nrf timer set ocmp
 *
 * Set the OCMP used by the timer to the desired expiration tick
 *
 * NOTE: Must be called with interrupts disabled.
 *
 * @param timer Pointer to timer.
 */
static void
cc26x0_timer_set_ocmp(struct cc26x0_hal_timer *bsptimer, uint32_t expiry)
{
    gpt_reg_t *hwtimer;

    hwtimer = bsptimer->tmr_reg;

    /* Disable ocmp interrupt and set new value */
    hwtimer->IMR &= ~(GPT_IMR_TAMIM);

    /* Set TAMR->TAMIE to enable match interrupt */
    hwtimer->TAMR |= GPT_TXMR_TXMIE;


    /* Set TAMATCHR to the value we want (expiry) */
    hwtimer->TAMATCHR = expiry;

    /* Set IMR->TAMRIS to unmask that interrupt */
    hwtimer->IMR |= GPT_IMR_TAMIM;

    /* Force interrupt to occur as we may have missed it */
    if ((int32_t)(cc26x0_read_timer_cntr(hwtimer) - expiry) >= 0) {
        NVIC_SetPendingIRQ(bsptimer->tmr_irq_num);
    }
}

/* Disable output compare used for timer */
static void
cc26x0_timer_disable_ocmp(gpt_reg_t *hwtimer)
{
    /* Disable ocmp interrupt and set new value */
    hwtimer->IMR &= ~(GPT_IMR_TAMIM);

    /* Set TAMR->TAMIE to enable match interrupt */
    hwtimer->TAMR &= ~(GPT_TXMR_TXMIE);
}

#if (MYNEWT_VAL(TIMER_0) || MYNEWT_VAL(TIMER_1) || MYNEWT_VAL(TIMER_2) || \
     MYNEWT_VAL(TIMER_3) || MYNEWT_VAL(TIMER_4) || MYNEWT_VAL(TIMER_5))
/**
 * hal timer chk queue
 *
 *
 * @param bsptimer
 */
#ifdef NOTDEFINED
static void
hal_timer_chk_queue(struct cc26x0_hal_timer *bsptimer)
{
    int32_t delta;
    uint32_t tcntr;
    uint32_t ctx;
    struct hal_timer *timer;

    /* disable interrupts */
    __HAL_DISABLE_INTERRUPTS(ctx);
    while ((timer = TAILQ_FIRST(&bsptimer->hal_timer_q)) != NULL) {
        
        tcntr = cc26x0_read_timer_cntr(bsptimer->tmr_reg);
        delta = 0;

        if ((int32_t)(tcntr - timer->expiry) >= delta) {
            TAILQ_REMOVE(&bsptimer->hal_timer_q, timer, link);
            timer->link.tqe_prev = NULL;
            timer->cb_func(timer->cb_arg);
        } else {
            break;
        }
    }

    /* Any timers left on queue? If so, we need to set OCMP */
    timer = TAILQ_FIRST(&bsptimer->hal_timer_q);
    if (timer) {
        cc26x0_timer_set_ocmp(bsptimer, timer->expiry);
    } else {
        cc26x0_timer_disable_ocmp(bsptimer->tmr_reg);
    }
    __HAL_ENABLE_INTERRUPTS(ctx);
}
#endif
#endif

/**
 * hal timer irq handler
 *
 * Generic HAL timer irq handler.
 *
 * @param tmr
 */
/**
 * hal timer irq handler
 *
 * This is the global timer interrupt routine.
 *
 */
#if (MYNEWT_VAL(TIMER_0) || MYNEWT_VAL(TIMER_1) || MYNEWT_VAL(TIMER_2) || \
     MYNEWT_VAL(TIMER_3) || MYNEWT_VAL(TIMER_4))

static void
hal_timer_irq_handler(struct cc26x0_hal_timer *bsptimer)
{
    gpt_reg_t *hwtimer;

    os_trace_enter_isr();

    /* Check interrupt source. If set, clear them */
    hwtimer = bsptimer->tmr_reg;

    if((hwtimer->MIS & GPT_MIS_TATOMIS) == GPT_MIS_TATOMIS){
        hwtimer->ICLR = GPT_ICLR_TATOCINT;
    }

    /* XXX: make these stats? */
    /* Count # of timer isrs */
    ++bsptimer->timer_isrs;

    /*
     * NOTE: we dont check the 'compare' variable here due to how the timer
     * is implemented on this chip. There is no way to force an output
     * compare, so if we are late setting the output compare (i.e. the timer
     * counter is already passed the output compare value), we use the NVIC
     * to set a pending interrupt. This means that there will be no compare
     * flag set, so all we do is check to see if the compare interrupt is
     * enabled.
     */
#ifdef NOTDEFINED
    if (hwtimer->INTENCLR & NRF_TIMER_INT_MASK(NRF_TIMER_CC_INT)) {
        hal_timer_chk_queue(bsptimer);
        /* XXX: Recommended by nordic to make sure interrupts are cleared */
        compare = hwtimer->EVENTS_COMPARE[NRF_TIMER_CC_INT];
    }
#endif
    os_trace_exit_isr();
}
#endif

#if MYNEWT_VAL(TIMER_0)
void
cc26x0_timer0_irq_handler(void)
{
    hal_timer_irq_handler(&cc26x0_hal_timer0);
}
#endif

#if MYNEWT_VAL(TIMER_1)
void
cc26x0_timer1_irq_handler(void)
{
    hal_timer_irq_handler(&cc26x0_hal_timer1);
}
#endif

#if MYNEWT_VAL(TIMER_2)
void
cc26x0_timer2_irq_handler(void)
{
    hal_timer_irq_handler(&cc26x0_hal_timer2);
}
#endif

#if MYNEWT_VAL(TIMER_3)
void
cc26x0_timer3_irq_handler(void)
{
    hal_timer_irq_handler(&cc26x0_hal_timer3);
}
#endif

/**
 * hal timer init
 *
 * Initialize platform specific timer items
 *
 * @param timer_num     Timer number to initialize
 * @param cfg           Pointer to platform specific configuration
 *
 * @return int          0: success; error code otherwise
 */
int
hal_timer_init(int timer_num, void *cfg)
{
    int rc;
    uint8_t irq_num;
    struct cc26x0_hal_timer *bsptimer;
    void *hwtimer;
    hal_timer_irq_handler_t irq_isr;

    CC26X0_HAL_TIMER_RESOLVE(timer_num, bsptimer);

    /* If timer is enabled do not allow init */
    if (bsptimer->tmr_enabled) {
        rc = EINVAL;
        goto err;
    }

    switch (timer_num) {
#if MYNEWT_VAL(TIMER_0)
    case 0:
        irq_num = GPTIMER_0A_IRQN;
        hwtimer = GPT0;
        irq_isr = cc26x0_timer0_irq_handler;
        break;
#endif
#if MYNEWT_VAL(TIMER_1)
    case 1:
        irq_num = GPTIMER_1A_IRQN;
        hwtimer = GPT1;
        irq_isr = cc26x0_timer1_irq_handler;
        break;
#endif
#if MYNEWT_VAL(TIMER_2)
    case 2:
        irq_num = GPTIMER_2A_IRQN;
        hwtimer = GPT2;
        irq_isr = cc26x0_timer2_irq_handler;
        break;
#endif
#if MYNEWT_VAL(TIMER_3)
    case 3:
        irq_num = GPTIMER_3A_IRQN;
        hwtimer = GPT3;
        irq_isr = cc26x0_timer3_irq_handler;
        break;
#endif
    default:
        hwtimer = NULL;
        break;
    }

    if (hwtimer == NULL) {
        rc = EINVAL;
        goto err;
    }

    /* Turn on the peripheral power domain */
    PRCM->PDCTL0 |= (1 << 2);
    while(!(PRCM->PDSTAT0PERIPH)){
        ;
    }

    PRCM->CLKLOADCTL = CLKLOADCTL_LOAD;

    while(((PRCM->CLKLOADCTL & CLKLOADCTL_LOADDONE)!=CLKLOADCTL_LOADDONE)){
        ;
    }
    
    /* Ensure that the clocks are configured properly */
    PRCM->GPTCLKDIV = CC26X0_MAX_TIMER_FREQ_PRESCALER_CODE;
    if(timer_num < 4){
        PRCM->GPTCLKGR |= (1 << timer_num);
        PRCM->GPTCLKGS |= (1 << timer_num);
    }
    else{
        rc = EINVAL;
        goto err;
    }

    PRCM->CLKLOADCTL = CLKLOADCTL_LOAD;

    while(((PRCM->CLKLOADCTL & CLKLOADCTL_LOADDONE)!=CLKLOADCTL_LOADDONE)){
        ;
    }

    bsptimer->tmr_reg = hwtimer;
    bsptimer->tmr_irq_num = irq_num;

    /* Disable IRQ, set priority and set vector in table */
    NVIC_DisableIRQ(irq_num);
    NVIC_SetPriority(irq_num, (1 << __NVIC_PRIO_BITS) - 1);
    NVIC_SetVector(irq_num, (uint32_t)irq_isr);

    return 0;

err:
    return rc;
}

/**
 * hal timer config
 *
 * Configure a timer to run at the desired frequency. This starts the timer.
 *
 * @param timer_num
 * @param freq_hz
 *
 * @return int
 */
int
hal_timer_config(int timer_num, uint32_t freq_hz)
{
    int rc;
    uint8_t prescaler;
    uint32_t ctx;
    uint32_t div;
    uint32_t min_delta;
    uint32_t max_delta;
    struct cc26x0_hal_timer *bsptimer;
    gpt_reg_t *hwtimer;

    CC26X0_HAL_TIMER_RESOLVE(timer_num, bsptimer);

    /* Set timer to desired frequency */
    div = CC26X0_MAX_TIMER_FREQ / freq_hz;

    /*
     * Largest prescaler is 2^8 and must make sure frequency not too high.
     * If hwtimer is NULL it means that the timer was not initialized prior
     * to call.
     */
    if (bsptimer->tmr_enabled || (div == 0) || (div > 256) ||
        (bsptimer->tmr_reg == NULL)) {
        rc = EINVAL;
        goto err;
    }

    if (div == 1) {
        prescaler = 0;
    } else {
        /* Find closest prescaler */
        for (prescaler = 1; prescaler < 9; ++prescaler) {
            if (div <= (1 << prescaler)) {
                min_delta = div - (1 << (prescaler - 1));
                max_delta = (1 << prescaler) - div;
                if (min_delta < max_delta) {
                    prescaler -= 1;
                }
                break;
            }
        }
    }

    /* Now set the actual frequency */
    bsptimer->tmr_freq = CC26X0_MAX_TIMER_FREQ / (1 << prescaler);
    bsptimer->tmr_enabled = 1;

    /* disable interrupts */
    __HAL_DISABLE_INTERRUPTS(ctx);

    /* Make sure HFXO is started */
    if ((DDI_0_OSC->STAT1 & DDI_0_OSC_STAT1_SCLK_HF_GOOD_MASK) != 
         DDI_0_OSC_STAT1_SCLK_HF_GOOD_MASK){
        //Start the clock up but this worries me
        //Also need to check if timer was enabled
        assert(0);
    }

    hwtimer = (gpt_reg_t *)(bsptimer->tmr_reg);

    /* STARTUP PROCEDURE FOR ONE-SHOT and PERIODIC TIMERS 
     * 1. Ensure the timer is disabled by clearing the 
     *    GPT:CTL TnEN register bit (n is either A or B)
     * 2. Write the GPTM Configuration Register GPT:CFG
     *    with a value of 0x00000000
     * 3. Configure the GPTM Timer n Mode Register (GPT:TnMR)
     *    with the desired bit:
     *     (a) Write a value of 0x1 for one-shot mode
     *     (b) write a value of 0x2 for periodic mode
     * 4. Optionally, configure the GPT:TnMR TnSNAPS, TnWOT,
     *    TnMTE, and TnCDIR register bits to select whether
     *    to capture the value of the free-running timer at
     *    time-out, use an external trigger to start counting,
     *    configure an additional trigger or interrupt, 
     *    and count up or down.
     * 5. Load the start value into the GPTM TImer n Interval
     *    Load Register (GPT:TnILR)
     * 6. If interrupts are required, set the appropriate 
     *    bits in the GPTM Interrupt Mask Register (GPT:IMR)
     * 7. Set the GPT:CTL TnEN register bit to enable the timer
     *    and start counting
     * 8. Poll the GPT:MRIS register or wait for the interrupt
     *    to be generated (if enabled). In both cases, the 
     *    status flags are cleared by writing a 1 to the appro-
     *    priate bit of the GPTM Interrupt Clear Register (GPT:ICR).
     */

    /* Stop the timer first */
    hwtimer->CTL &= ~(GPT_CTL_TAEN);

    /* Clear the timer config register */
    hwtimer->CFG = 0x0;

    /* We have to be in 16-bit mode to use the prescaler */
    hwtimer->CFG = GPT_CFG_16T;

    /* Configure for Periodic Operation
     * and counting up
     */

    hwtimer->TAMR = GPT_TXMR_TXMR_PERIODIC | GPT_TXMR_TXCDIR_UP;

    /* Set the pre-scalar */
    hwtimer->TAPR = prescaler;

    /* Enable Interrupt Generation */
    //hwtimer->IMR |= GPT_IMR_TATOIM;

    /* Start the timer */
    hwtimer->CTL |= GPT_CTL_TAEN;

    /*Enable interrupts*/
    NVIC_EnableIRQ(bsptimer->tmr_irq_num);

    __HAL_ENABLE_INTERRUPTS(ctx);

    return 0;

err:
    return rc;
}

/**
 * hal timer deinit
 *
 * De-initialize a HW timer.
 *
 * @param timer_num
 *
 * @return int
 */

/*
 * TODO: If we are deinit the last timer, shut off the whole peripheral 
 *       to save power
 */
int
hal_timer_deinit(int timer_num)
{
    int rc;
    uint32_t ctx;
    struct cc26x0_hal_timer *bsptimer;
    gpt_reg_t *hwtimer;

    rc = 0;
    CC26X0_HAL_TIMER_RESOLVE(timer_num, bsptimer);

    __HAL_DISABLE_INTERRUPTS(ctx);
    
    hwtimer = (gpt_reg_t *)bsptimer->tmr_reg;
    hwtimer->IMR &= ~(GPT_IMR_TATOIM);
    hwtimer->CTL &= ~(GPT_CTL_TAEN);

    bsptimer->tmr_enabled = 0;
    bsptimer->tmr_reg = NULL;
    __HAL_ENABLE_INTERRUPTS(ctx);

err:
    return rc;
}

/**
 * hal timer get resolution
 *
 * Get the resolution of the timer. This is the timer period, in nanoseconds
 *
 * @param timer_num
 *
 * @return uint32_t The
 */
uint32_t
hal_timer_get_resolution(int timer_num)
{
    int rc;
    uint32_t resolution;
    struct cc26x0_hal_timer *bsptimer;

    CC26X0_HAL_TIMER_RESOLVE(timer_num, bsptimer);

    resolution = 1000000000 / bsptimer->tmr_freq;
    return resolution;

err:
    rc = 0;
    return rc;
}

/**
 * hal timer read
 *
 * Returns the timer counter. NOTE: if the timer is a 16-bit timer, only
 * the lower 16 bits are valid. If the timer is a 64-bit timer, only the
 * low 32-bits are returned.
 *
 * @return uint32_t The timer counter register.
 */
uint32_t
hal_timer_read(int timer_num)
{
    int rc;
    uint32_t tcntr;
    struct cc26x0_hal_timer *bsptimer;

    CC26X0_HAL_TIMER_RESOLVE(timer_num, bsptimer);
    
    tcntr = cc26x0_read_timer_cntr(bsptimer->tmr_reg);


    return tcntr;

    /* Assert here since there is no invalid return code */
err:
    assert(0);
    rc = 0;
    return rc;
}

/**
 * hal timer delay
 *
 * Blocking delay for n ticks
 *
 * @param timer_num
 * @param ticks
 *
 * @return int 0 on success; error code otherwise.
 */
int
hal_timer_delay(int timer_num, uint32_t ticks)
{
    uint32_t until;

    until = hal_timer_read(timer_num) + ticks;
    while ((int32_t)(hal_timer_read(timer_num) - until) <= 0) {
        /* Loop here until finished */
    }

    return 0;
}

/**
 *
 * Initialize the HAL timer structure with the callback and the callback
 * argument. Also initializes the HW specific timer pointer.
 *
 * @param cb_func
 *
 * @return int
 */
int
hal_timer_set_cb(int timer_num, struct hal_timer *timer, hal_timer_cb cb_func,
                 void *arg)
{
    int rc;
    struct cc26x0_hal_timer *bsptimer;

    CC26X0_HAL_TIMER_RESOLVE(timer_num, bsptimer);

    timer->cb_func = cb_func;
    timer->cb_arg = arg;
    timer->link.tqe_prev = NULL;
    timer->bsp_timer = bsptimer;

    rc = 0;

err:
    return rc;
}

int
hal_timer_start(struct hal_timer *timer, uint32_t ticks)
{
    int rc;
    uint32_t tick;
    struct cc26x0_hal_timer *bsptimer;

    /* Set the tick value at which the timer should expire */
    bsptimer = (struct cc26x0_hal_timer *)timer->bsp_timer;
    
    tick = cc26x0_read_timer_cntr(bsptimer->tmr_reg) + ticks;

    rc = hal_timer_start_at(timer, tick);
    return rc;
}

int
hal_timer_start_at(struct hal_timer *timer, uint32_t tick)
{
    uint32_t ctx;
    struct hal_timer *entry;
    struct cc26x0_hal_timer *bsptimer;

    if ((timer == NULL) || (timer->link.tqe_prev != NULL) ||
        (timer->cb_func == NULL)) {
        return EINVAL;
    }
    bsptimer = (struct cc26x0_hal_timer *)timer->bsp_timer;
    timer->expiry = tick;

    __HAL_DISABLE_INTERRUPTS(ctx);

    if (TAILQ_EMPTY(&bsptimer->hal_timer_q)) {
        TAILQ_INSERT_HEAD(&bsptimer->hal_timer_q, timer, link);
    } else {
        TAILQ_FOREACH(entry, &bsptimer->hal_timer_q, link) {
            if ((int32_t)(timer->expiry - entry->expiry) < 0) {
                TAILQ_INSERT_BEFORE(entry, timer, link);
                break;
            }
        }
        if (!entry) {
            TAILQ_INSERT_TAIL(&bsptimer->hal_timer_q, timer, link);
        }
    }

    /* If this is the head, we need to set new OCMP */
    if (timer == TAILQ_FIRST(&bsptimer->hal_timer_q)) {
        cc26x0_timer_set_ocmp(bsptimer, timer->expiry);
    }

    __HAL_ENABLE_INTERRUPTS(ctx);

    return 0;
}

/**
 * hal timer stop
 *
 * Stop a timer.
 *
 * @param timer
 *
 * @return int
 */
int
hal_timer_stop(struct hal_timer *timer)
{
    uint32_t ctx;
    int reset_ocmp;
    struct hal_timer *entry;
    struct cc26x0_hal_timer *bsptimer;

    if (timer == NULL) {
        return EINVAL;
    }

   bsptimer = (struct cc26x0_hal_timer *)timer->bsp_timer;

    __HAL_DISABLE_INTERRUPTS(ctx);

    if (timer->link.tqe_prev != NULL) {
        reset_ocmp = 0;
        if (timer == TAILQ_FIRST(&bsptimer->hal_timer_q)) {
            /* If first on queue, we will need to reset OCMP */
            entry = TAILQ_NEXT(timer, link);
            reset_ocmp = 1;
        }
        TAILQ_REMOVE(&bsptimer->hal_timer_q, timer, link);
        timer->link.tqe_prev = NULL;
        if (reset_ocmp) {
            if (entry) {
                cc26x0_timer_set_ocmp((struct cc26x0_hal_timer *)entry->bsp_timer,
                                   entry->expiry);
            } else {
                cc26x0_timer_disable_ocmp(bsptimer->tmr_reg);

            }
        }
    }

    __HAL_ENABLE_INTERRUPTS(ctx);

    return 0;
}
