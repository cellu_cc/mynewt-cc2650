/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <assert.h>
#include "hal/hal_watchdog.h"
#include "bsp/cmsis_nvic.h"
#include "os/os_trace_api.h"
#include "mcu/cc26x0.h"

/*
static void
cc26x0_hal_unlock_wdt(){
    while(WDT->LOCK){
        WDT->LOCK = 0x1ACCE551
    }

}
*/

static void
cc26x0_hal_wdt_default_handler(void)
{
    assert(0);
}

/**@brief WDT interrupt handler. */
static void
cc26x0_wdt_irq_handler(void)
{
    os_trace_enter_isr();
    if (WDT->MIS & 0x1) {
        WDT->LOAD = 0;
        cc26x0_hal_wdt_default_handler();
    }
    os_trace_exit_isr();
}

int
hal_watchdog_init(uint32_t expire_msecs)
{
    uint64_t expiration;

    //WDT->CONFIG = WDT_CONFIG_SLEEP_Msk;

    /* Convert msec timeout to counts of a 32768 crystal */
    expiration = ((uint64_t)expire_msecs * 32768) / 1000;
    WDT->LOAD = (uint32_t)expiration;

    NVIC_SetVector(WATCHDOG_IRQN, (uint32_t) cc26x0_wdt_irq_handler);
    NVIC_SetPriority(WATCHDOG_IRQN, (1 << __NVIC_PRIO_BITS) - 1);
    NVIC_ClearPendingIRQ(WATCHDOG_IRQN);
    NVIC_EnableIRQ(WATCHDOG_IRQN);

    return (0);
}

void
hal_watchdog_enable(void)
{
    WDT->CTL = (WDT->CTL)|0x1;
}
/*
 * Writing any value to the ICR register will 
 * cause the counter to reset to the value in
 * LOAD
 */
void
hal_watchdog_tickle(void)
{
    WDT->ICR = 0x1;
}
